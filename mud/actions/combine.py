# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import CombineOnEvent, CombineOffEvent, CombineWithEvent

class CombineOnAction(Action2):
    EVENT = CombineOnEvent
    ACTION = "light-on"
    RESOLVE_OBJECT = "resolve_for_use"

class CombineOffAction(Action2):
    EVENT = CombineOffEvent
    ACTION = "light-off"
    RESOLVE_OBJECT = "resolve_for_use"

class CombineWithAction(Action3):
    EVENT = CombineWithEvent
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "light-on-with"