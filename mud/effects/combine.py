# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import CombineOnEvent, CombineOffEvent, CombineWithEvent

class CombineOnEffect(Effect2):
    EVENT = CombineOnEvent

class CombineOffEffect(Effect3):
    EVENT = CombineOffEvent

class CombineWithEffect(Effect3):
    EVENT = CombineWithEvent